<?php

namespace Test;

use Exception;
use GetRepo\MusicDownloader\Downloader;
use PHPUnit\Framework\TestCase;

class DownloaderTest extends TestCase
{
    /** @var Downloader */
    private Downloader $downloader;

    public static function getDefaultSavePath(): string
    {
        return sys_get_temp_dir() . '/music-downloader';
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function setUp(): void
    {
        include dirname(__DIR__) . '/bootstrap.php';
        /** @var \Symfony\Component\DependencyInjection\ContainerBuilder $containerBuilder */
        $this->downloader = $containerBuilder->get(Downloader::class);
    }

    public function data(): array
    {
        $defaultSavePath = self::getDefaultSavePath();

        return [
            'URL no match' => [
                new Exception("No config for 'https://www.whatever.com'"),
                'https://www.whatever.com',
            ],
            // youtube track NOT WORKING ANYMORE
            'youtube track : wrong video id' => [
                new Exception('youtube-dlp failed to fetch'),
                'https://www.youtube.com/watch?v=THIS_VIDEO_DOES_NO_EXISTS',
            ],
            'youtube track : Short video' => [
                ["{$defaultSavePath}/Shortest Video on Youtube.mp3"],
                'https://www.youtube.com/watch?v=tPEE9ZwTmy0',
            ],
            // bandcamp track
            'bandcamp track: wrong url' => [
                new Exception('Client error'),
                'https://starfrosch.bandcamp.com/track/this-track-does-not-exists',
            ],
            'bandcamp track: Short track from starfrosch' => [
                ["{$defaultSavePath}/STARFROSCH - Half Sine.mp3"],
                'https://starfrosch.bandcamp.com/track/half-sine',
            ],
            // bandcamp album
            'bandcamp album: wrong url' => [
                new Exception('Client error'),
                'https://starfrosch.bandcamp.com/album/this-album-does-not-exists',
            ],
            'bandcamp album: World Record - Shortest Track Ever' => [
                ["{$defaultSavePath}/ARMOR OF EROSION - №84: nomen nominandum/01 №84: nomen nominandum.mp3"],
                'https://armoroferosion.bandcamp.com/album/84-nomen-nominandum',
            ],
            // spotify album
            'spotify album: wrong url' => [
                new Exception('Client error'),
                'https://open.spotify.com/album/invalid-albun',
            ],
            'spotify track: QUEEN - Yeah Remastered 2011' => [
                [
                    "{$defaultSavePath}/QUEEN - Yeah - Remastered 2011.mp3",
                ],
                'https://open.spotify.com/track/5PbMSJZcNA3p2LZv7C56cm',
            ],
        ];
    }

    /**
     * @dataProvider data
     * @param mixed $expected
     */
    public function testDownloader(
        $expected,
        string $url,
        array $config = []
    ): void {
        // manage expects exception
        if ($expected instanceof Exception) {
            $this->expectException(get_class($expected));
            $this->expectExceptionMessage($expected->getMessage());
            $expected = null;
        }

        // prepare config for test
        if (!isset($config['save_path'])) {
            $config['save_path'] = self::getDefaultSavePath();
        }

        $config = $this->downloader->overrideConfig($config);
        $this->assertEquals($expected, $this->downloader->fetch($url));
        $this->assertIsArray($expected);
        foreach ($expected as $file) {
            $this->assertFileExists($file);
            $this->assertFileIsReadable($file);
            // cleaning
            @unlink($file);
        }
    }
}
