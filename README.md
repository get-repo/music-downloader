<p align="center">
    <img src="https://gitlab.com/uploads/-/system/project/avatar/11158479/musicdownloader_logo.png" height=100 />
</p>

<h1 align=center>Music Downloader</h1>

For a better offline music life !

## Table of Contents

1. [Installation](#installation)
1. [Usage](#usage)
1. [Music Sources](#music-sources)

## Installation

This is installable via [Composer](https://getcomposer.org/):

    composer config repositories.get-repo/music-downloader git https://gitlab.com/get-repo/music-downloader.git
    composer require get-repo/music-downloader

## Usage

```sh
Usage:
  downloader <url>

Arguments:
  url                   URL

Options:
  -h, --help            Display help for the given command. When no command is given display help for the downloader command
  -q, --quiet           Do not output any message
  -V, --version         Display this application version
  --ansi|--no-ansi      Force (or disable --no-ansi) ANSI output
  -n, --no-interaction  Do not ask any interactive question
  -v|vv|vvv, --verbose  Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug
```

## Music Sources
 - <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/80/Bandcamp-logotype-color.svg/800px-Bandcamp-logotype-color.svg.png" height="15px">
   Tracks & Albums
 - <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/26/Spotify_logo_with_text.svg/1920px-Spotify_logo_with_text.svg.png" height="22px" style="position: relative; top: 5px;">
   Tracks & Albums
- <img src="https://upload.wikimedia.org/wikipedia/commons/b/b8/YouTube_Logo_2017.svg" height="19px" style="position: relative; top: 4px;">
  Convert video to mp3
