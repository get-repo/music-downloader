<?php

namespace GetRepo\MusicDownloader\Configuration;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Configuration.
 */
class Configuration implements ConfigurationInterface
{
    private array $fetchers;

    public function __construct(array $fetchers)
    {
        $this->fetchers = $fetchers;
    }

    /**
     * {@inheritdoc}
     *
     * @see ConfigurationInterface::getConfigTreeBuilder()
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('config');
        /** @var ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();

        // @phpstan-ignore-next-line
        $rootNode
            ->children()
                ->scalarNode('save_path')
                    ->isRequired()
                    ->cannotBeEmpty()
                ->end()
                ->arrayNode('sites')
                    ->children()
                        ->append($this->siteDefinitions('tracks'))
                        ->append($this->siteDefinitions('albums'))
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }

    public function siteDefinitions(string $name): ArrayNodeDefinition
    {
        $treeBuilder = new TreeBuilder($name);
        /** @var ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();

        // @phpstan-ignore-next-line
        $node = $rootNode->arrayPrototype()
                ->children()
                    ->scalarNode('url')
                        ->isRequired()
                        ->cannotBeEmpty()
                        ->validate()
                            ->ifTrue(function ($regex) {
                                return false === @preg_match($regex, 'whatever');
                            })
                            ->thenInvalid("Invalid 'url' regular expression %surl")
                        ->end()
                    ->end()
                    ->arrayNode('fetchers')
                        ->scalarPrototype()->end()
                        ->validate()
                            ->always()
                            ->then(function ($fetchers) {
                                $diff = array_diff(array_keys($fetchers), $this->fetchers);
                                if ($diff) {
                                    throw new \InvalidArgumentException("Invalid fetchers: " . implode(', ', $diff));
                                }

                                return $fetchers;
                            })
                        ->end()
                    ->end()
                    ->arrayNode('filename')
                        ->children()
                            ->scalarNode('format')
                                ->isRequired()
                                ->cannotBeEmpty()
                            ->end()
                            ->arrayNode('selectors')
                                ->arrayPrototype()
                                    ->beforeNormalization()
                                        ->ifString()
                                        ->then(function ($selector) {
                                            return [
                                                'selector' => $selector,
                                                'cleaner' => null,
                                            ];
                                        })
                                    ->end()
                                    ->children()
                                        ->scalarNode('selector')
                                            ->isRequired()
                                            ->cannotBeEmpty()
                                        ->end()
                                        ->scalarNode('cleaner')->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();
        /** @var ArrayNodeDefinition $children */
        $children = current($rootNode->getChildNodeDefinitions());
        if ('albums' === $name) {
            // @phpstan-ignore-next-line
            $children
                ->children()
                    ->variableNode('tracks') ->end()
                    ->scalarNode('cover')
                        ->cannotBeEmpty()
                    ->end()
                ->end();
        }

        return $node;
    }
}
