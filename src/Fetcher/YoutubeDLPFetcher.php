<?php

namespace GetRepo\MusicDownloader\Fetcher;

use Symfony\Component\Process\Process;

class YoutubeDLPFetcher extends AbstractFetcher
{
    protected function doFetch(string $url): string
    {
        $process = new Process([
            dirname(dirname(__DIR__)) . '/vendor/bin/youtube-dlp',
            '-q',
            '--no-colors',
            '--geo-bypass',
            '--retries=3',
            '--dump-single-json',
            $url
        ]);
        $process->run();
        $json = json_decode($process->getOutput(), true);
        if (!$json) {
            throw new \Exception("youtube-dlp failed to fetch '{$url}'");
        }

        // get only audio format
        $audioFormats = array_filter($json['formats'], function ($format) {
            return false !== stripos($format['resolution'], 'audio');
        });
        usort($audioFormats, function ($a, $b) {
            return $b['quality'] - $a['quality'];
        });

        return $audioFormats[0]['url'];
    }
}
