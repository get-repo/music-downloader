<?php

namespace GetRepo\MusicDownloader\Fetcher;

class SpotifyDownFetcher extends AbstractFetcher
{
    protected function doFetch(string $url): string
    {
        $parts = explode('/', $url);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.spotifydown.com/download/' . end($parts));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Origin: https://spotifydown.com', 'Referer: https://spotifydown.com/']);
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            throw new \Exception("spotifydown.com CURL error: " . curl_error($ch));
        }
        curl_close($ch);

        $json = @json_decode($result, true);
        if (!is_array($json)) {
            throw new \Exception("spotifydown.com invalid JSON response '{$result}'");
        }
        if (!isset($json['link'])) {
            throw new \Exception("spotifydown.com JSON is missing the \"link\" key ('{$result}')");
        }

        // test if this download link has expired
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $json['link']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3);
        $result = curl_exec($ch);
        if (@json_decode($result, true)) { // if json then we have a "status": "error" response
            throw new \Exception("spotifydown.com failed to verify link: {$json['link']}");
        }
        curl_close($ch);

        return $json['link'];
    }
}
