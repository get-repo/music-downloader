<?php

namespace GetRepo\MusicDownloader\Fetcher;

use Symfony\Component\Process\Process;

class YoutubeDLCFetcher extends AbstractFetcher
{
    protected function doFetch(string $url): string
    {
        $process = new Process([
            '/usr/local/bin/youtube-dlc',
            '-q',
            '--no-colors',
            '--geo-bypass',
            '--retries=3',
            '--dump-single-json',
            $url
        ]);
        $process->run();
        $json = json_decode($process->getOutput(), true);
        if (!$json) {
            throw new \Exception("youtube-dlp failed to fetch '{$url}'");
        }

        // get only audio format
        $audioFormats = array_filter($json['formats'], function ($format) {
            return false !== stripos($format['format'], 'audio');
        });
        usort($audioFormats, function ($a, $b) {
            return $b['abr'] - $a['abr'];
        });

        return $audioFormats[0]['url'];
    }
}
