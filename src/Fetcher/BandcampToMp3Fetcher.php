<?php

namespace GetRepo\MusicDownloader\Fetcher;

class BandcampToMp3Fetcher extends AbstractFetcher
{
    protected function doFetch(string $url): string
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://bandcamptomp3.com/wp-json/aio-dl/video-data/');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "url=" . urlencode($url));
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/x-www-form-urlencoded',
        ]);

        $result = @curl_exec($ch);
        if (!$result) {
            throw new \Exception("bandcamptomp3.com failed to fetch '{$url}'");
        }
        $json = @json_decode($result, true);
        if (!is_array($json)) {
            throw new \Exception("bandcamptomp3.com invalid JSON response '{$json}'");
        }

        return $json['medias'][0]['url'];
    }
}
