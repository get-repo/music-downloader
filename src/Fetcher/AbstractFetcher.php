<?php

namespace GetRepo\MusicDownloader\Fetcher;

use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\String\ByteString;

abstract class AbstractFetcher
{
    private ?string $name = null;
    private AdapterInterface $cache;

    abstract protected function doFetch(string $url): string;

    public function __construct(AdapterInterface $cache)
    {
        $this->cache = $cache;
    }

    public function getName(): string
    {
        if (!$this->name) {
            $s = new ByteString(get_class($this));
            /** @var ByteString $s */
            $s = $s->replace(__NAMESPACE__ . '\\', '')
                ->replace('Fetcher', '');
            $this->name = (string) $s->snake();
        }

        return $this->name;
    }

    public function fetch(string $url, bool $useCache = false): string
    {
        $cacheItem = $this->cache->getItem("fetcher_{$url}");
        if ($useCache && $cacheItem->isHit()) {
            return $cacheItem->get();
        }

        $result = $this->doFetch($url);
        if ($useCache && $result) {
            $cacheItem->set($result);
            $this->cache->save($cacheItem);
        }

        return $result;
    }
}
