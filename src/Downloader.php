<?php

namespace GetRepo\MusicDownloader;

use GetRepo\ExpressionLanguage\ExpressionLanguage;
use GetRepo\MusicDownloader\Configuration\Configuration;
use GetRepo\MusicDownloader\Fetcher\AbstractFetcher;
use GuzzleHttp\Client;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\String\ByteString;
use Symfony\Component\Yaml\Yaml;
use Twig\Environment;
use Twig\Loader\ArrayLoader;

class Downloader
{
    private array $fetchers;
    private AdapterInterface $cache;
    private array $config;
    private ExpressionLanguage $el;
    private OutputInterface $output;

    public function __construct(RewindableGenerator $fetchers, AdapterInterface $cache)
    {
        // prepare fetchers
        foreach ($fetchers as $fetcher) {
            $this->fetchers[$fetcher->getName()] = $fetcher;
        }
        // cache
        $this->cache = $cache;
        // expression language
        $this->el = new ExpressionLanguage();
        // default output
        $this->output = new NullOutput();
    }

    public function setOutput(OutputInterface $output): self
    {
        $this->output = $output;

        return $this;
    }

    public function overrideConfig(array $overrides): self
    {
        // prepare config
        $config = Yaml::parseFile(dirname(__DIR__) . '/config.yml');
        $parameters = Yaml::parseFile(dirname(__DIR__) . '/parameters.yml');
        $processor = new Processor();
        $databaseConfiguration = new Configuration(array_keys($this->fetchers));
        $this->config = $processor->processConfiguration(
            $databaseConfiguration,
            [$config, $parameters['parameters'], $overrides]
        );

        return $this;
    }

    public function fetch(string $url, bool $useCache = false): array
    {
        $filesystem = new Filesystem();
        $config = $this->getConfig($url);
        if (!isset($this->_config)) {
            $this->output->writeln("fetch from <comment>{$config['site_name']} {$config['type']}</>");
        }

        switch ($config['type']) {
            case 'tracks':
                $filename = $this->getFilename($url);
                $filename = "{$this->config['save_path']}/{$filename}.mp3";

                $fetcher = $this->getFetcher($url);
                $audioUrl = $fetcher->fetch($url, $useCache);
                $this->output->write(' .');
                if (!is_dir($this->config['save_path']) && !@mkdir($this->config['save_path'])) {
                    throw new \LogicException("Failed to create save path dir '{$this->config['save_path']}'.");
                }
                $this->output->write('.');
                $filesystem->mkdir(dirname($filename));
                $this->output->write('.');
                $copyCallback = [$fetcher, 'copy'];
                if (is_callable($copyCallback)) {
                    call_user_func($copyCallback, $audioUrl, $filename);
                } else {
                    @copy($audioUrl, $filename);
                }
                if (0 === filesize($filename)) {
                    throw new \LogicException(sprintf('File could not be downloaded from "%s"', $audioUrl));
                }
                $this->output->writeLn(' <info>OK</>');
                return [$filename];
            case 'albums':
                // parse tracks with web page content
                $content = $this->getWebPage($url);
                $tracks = $this->el->evaluate($config['tracks']['collection'], [
                    'content' => $content,
                    'crawler' => new Crawler($content),
                    'url' => $url,
                ]);
                $count = $tracks->count();
                if (!$count) {
                    throw new \LogicException("No tracks found.");
                }
                $this->output->writeln("<info>{$count}</> tracks");
                $filenames = [];
                $tracks->each(function (Crawler $item, $i) use ($url, $config, &$filenames, $content, $useCache) {
                    $this->output->writeln("");
                    $trackUrl = $this->el->evaluate($config['tracks']['item'], [
                        'item' => $item,
                        'url' => $url,
                        'parsed_url' => parse_url($url),
                    ]);
                    $this->_config = $config; // @phpstan-ignore-line
                    $this->_config['type'] = 'tracks';
                    $this->_config['album_content'] = $content;
                    $this->_config['filename']['selectors']['track'] = [
                        'selector' => sprintf('"%02d"', $i + 1),
                        'cleaner' => null,
                    ];
                    $filenames = array_merge($filenames, $this->fetch($trackUrl, $useCache));
                });
                if ($filenames) {
                    $this->output->write("\ncover ...");
                    // cover
                    $cover = $this->el->evaluate($config['cover'], [
                        'content' => $content,
                        'crawler' => new Crawler($content),
                        'url' => $url,
                    ]);
                    copy($cover, dirname($filenames[0]) . '/cover.jpg');
                    $this->output->writeln(" <info>OK</>");
                }
                return $filenames;
        }

        throw new \Exception("Invalid type '{$config['type']}'");
    }

    private function getFilename(string $url): string
    {
        // config for URL
        $config = $this->getConfig($url);
        // web page content
        $content = $this->getWebPage($url);

        $parts = [];
        foreach ($config['filename']['selectors'] as $name => $selectorConf) {
            try {
                $value = $this->el->evaluate($selectorConf['selector'], [
                    'content' => $content,
                    'album_content' => $config['album_content'] ?? false,
                    'crawler' => new Crawler($content),
                    'crawler_album' => new Crawler($config['album_content'] ?? ''),
                    'url' => $url,
                ]);
            } catch (\Exception $e) {
                throw new \Exception("Failed to parse '{$name}'");
            }
            $value = trim($value);
            if ($selectorConf['cleaner']) {
                $value = $this->el->evaluate($selectorConf['cleaner'], [
                    'content' => $content,
                    'value' => $value,
                    's' => new ByteString($value),
                    'url' => $url,
                ]);
                $value = trim($value);
            }
            $parts[$name] = str_replace(['/'], ' ', $value);
            $this->output->writeln("{$name}: <info>{$parts[$name]}</>");
        }

        // creater filename with twig
        $twig = new Environment(
            new ArrayLoader(['filename' => $config['filename']['format']]),
            ['autoescape' => false]
        );

        return $twig->render('filename', $parts);
    }

    private function getWebPage(string $url, bool $useCache = false): string
    {
        if (!isset($this->_config)) {
            $this->output->write('crawling ...');
        }
        $cacheItem = $this->cache->getItem("content_{$url}");
        if ($useCache && $cacheItem->isHit()) {
            $content = $cacheItem->get();
        } else {
            $client = new Client();
            $response = $client->request('GET', $url);
            $content = (string)$response->getBody();
            if ($useCache) {
                $cacheItem->set($content);
                $this->cache->save($cacheItem);
            }
        }
        if (!isset($this->_config)) {
            $this->output->writeln(" <info>OK</>");
        }
        return $content;
    }

    private function getFetcher(string $url): AbstractFetcher
    {
        $config = $this->getConfig($url);
        $this->output->writeln("Fetching:");
        foreach (array_keys($config['fetchers']) as $fetcherName) {
            $this->output->write("  {$fetcherName}");
            if (!isset($this->fetchers[$fetcherName])) {
                throw new \LogicException("Fetcher '{$fetcherName}' not found.");
            }
            return $this->fetchers[$fetcherName];
        }

        throw new \LogicException("No fetcher for '{$url}'.");
    }

    private function getConfig(string $url): array
    {
        if (isset($this->_config['url'])) {
            return $this->_config;
        }

        if (!isset($this->config)) {
            // just to prepare init config
            $this->overrideConfig([]);
        }

        foreach (array_keys($this->config['sites']) as $type) {
            foreach ($this->config['sites'][$type] as $siteName => $site) {
                if (@preg_match($site['url'], $url)) {
                    $site['type'] = $type;
                    $site['site_name'] = $siteName;
                    return $site;
                }
            }
        }

        throw new \Exception("No config for '{$url}'.");
    }
}
